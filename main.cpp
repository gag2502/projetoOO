#include <iostream>
#include "pessoa.hpp"

int main (int argc, char ** argv) {

          Pessoa pessoa_1;
          Pessoa pessoa_2;
          Pessoa *pessoa_3;
         
          pessoa_3 = new Pessoa();
         
          cout << "Construtor padrao" << endl;
          cout << "Nome:" << pessoa_1.getNome() << endl;
          cout << "Telefone:" << pessoa_1.getTelefone() << endl;
          cout << "Idade:" << pessoa_1.getIdade() << endl;
           
	  pessoa_1.setNome("João");
          pessoa_1.setTelefone("555-2222");
          pessoa_1.setIdade(23);
         
          pessoa_2.setNome("Maria");
          pessoa_2.setTelefone("123-1232");
          pessoa_2.setIdade(24);
          
          pessoa_3->setNome("Ana");
          pessoa_3->setTelefone("222-2233");
          pessoa_3->setIdade(22);

          cout << "Pessoa 1" << endl;
          cout << "Nome:" << pessoa_1.getNome() << endl;
          cout << "Telefone:" << pessoa_1.getTelefone() << endl;
          cout << "Idade:" << pessoa_1.getIdade() << endl;

          cout << "Pessoa 2" << endl;
          cout << "Nome:" << pessoa_2.getNome() << endl;
          cout << "Telefone:" << pessoa_2.getTelefone() << endl;
          cout << "Idade:" << pessoa_2.getIdade() << endl;
          
          cout << "Pessoa 3" << endl;
          cout << "Nome:" << pessoa_3->getNome() << endl;
          cout << "Telefone:" << pessoa_3->getTelefone() << endl;
          cout << "Idade:" << pessoa_3->getIdade() << endl;
          
          delete(pessoa_3);
          
}

